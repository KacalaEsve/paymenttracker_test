# PaymentTracker
BSC Task

Build

Apache Maven: 4.X.X

Java: 1.7

Go to project folder and build with "mvn clean install" or open in UI tool and build there.  

Run

java -jar payment-tracker-1.0-SNAPSHOT.jar 

or

java -jar payment-tracker-1.0-SNAPSHOT.jar payments-file-path

payments-file-path you can type to console after run.

paymentTracker

Reading from any payments-file containing data structure like:

USD 1000

HKD 100

USD -100

RMB 2000

HKD 200



"quit" command shut downs the application

RATE is applied if there is defined CURRENCY in ExchangeService

OUTPUT:

USD 900

RMB 2000 (USD 314.60)

HKD 300 (USD 38.62)



NOTE: Tested on OS Windows ONLY.



