/**
 * 
 */
package net.esve.bsc.model;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.is;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.BeforeClass;

import org.junit.Test;


/**
 * @author Viliam
 *
 */

public class PaymentRecordsTest {

	private static final List<Payment> payments = new ArrayList<>(Arrays.asList(new Payment("EUR", new BigDecimal(100)),new Payment("CZK", new BigDecimal(200)),new Payment("USD", new BigDecimal(300))));

	private static Map<String, BigDecimal> paymentMap = new HashMap<>();


	/**
	 * Test method for
	 * {@link net.esve.bsc.model.PaymentRecords#setPayments(net.esve.bsc.model.Payment)}
	 * .
	 */
	@Test
	public void testSetPayments() {
		for (Payment payment : payments) {
			paymentMap.put(payment.getCurrency(), payment.getAmount());
        }
		
		assertEquals(paymentMap.isEmpty(), false);
	}
	

	/**
	 * Test method for {@link net.esve.bsc.model.PaymentRecords#getPayments()}.
	 */
	@Test
	public void testGetPayments() {
		assertEquals(paymentMap.isEmpty(), false);
	}
	
	/**
	 * Test method for {@link net.esve.bsc.model.PaymentRecords#getInstance()}.
	 */
	@Test
	public void testGetInstance() {
		for (Payment payment : payments) {
			PaymentRecords.getInstance().putPayment(payment);
			paymentMap.put(payment.getCurrency(), payment.getAmount());
	     }	
	
		assertEquals(PaymentRecords.getInstance().getPayments(), paymentMap);
	}

}
